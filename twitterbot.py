import tweepy
import string
import re
from nltk.corpus import words
import random
import yodify2 as yodify
import contractions
from datetime import datetime, timedelta
import sqlite3

sw_words = [
    'yoda', 'luke', 'han', 'jango', 'fett', 'skywalker', 'boba', 'yodifier'
]

all_words = contractions.get_words(extras=sw_words)

def rem_punc(w):
    for p in '!,.?";:' + "'":
        w = w.replace(p, '')
    return w

def is_clean_tweet(tweet):
    print('\t Checking if valid: "{}"'.format(tweet))
    for word in tweet.split():
        if 'http' in word or 'www' in word:
            print('\t\tInvalid tweet: has URL')
            return False
    if len(re.split('(?<=[.!?]) +', tweet)) > 2:
        print('\t\tInvalid tweet: bad sentence count =',
            re.split('(?<=[.!?]) +', tweet))
        return False
    for word in tweet.lower().split():
        if rem_punc(word) not in all_words and word[0] not in '#@':
            print('\t\tInvalid tweet: bad word =', word)
            return False
    return 'RT' not in tweet

def do_cleaning(tw):
    tweet = ''.join(x for x in tw.text if x in string.printable)
    words = tweet.split()
    while words[0][0] in '#@':
        words = words[1:]
    while words[-1][0] in '#@':
        words = words[:-1]
    words = [w.replace('#', '').replace('@', '') for w in words]
    tw.text = ' '.join(words)
    return tw

def get_tweeters_username(tweet):
    return '@' + tweet.user.screen_name

def respond_to_tweet(api, tweet, response):
    m = ' '.join([get_tweeters_username(tweet), response])
    print('RESPONSE:\n', m)
    api.update_status(m, tweet.id)

def is_tweet_new(conn, tweet):
    cursor = conn.execute("SELECT ID FROM TWEETS WHERE ID=" + str(tweet.id))
    return len([r for r in cursor]) == 0

# Consumer Key (API Key)
cons_key = 'eosNcEqCtsNmX59ytRZQzFLBF'
# Consumer Secret (API Secret)
cons_secret = '8pIBQRkydB2I0EM2qXjHDCyBO1olNotGvTI8Av0jzLFy3RMgXO'
# Access Token
access_token = '891757700979150849-4mmpkhsHOU96DUmS7WHFws7yvUcnN2y'
# Access Token Secret
access_token_secret = 'b97WRcArpIqA3wEHYokwqyMYg092v9l5aYH9frHhQYqPF'

# login
auth = tweepy.OAuthHandler(cons_key, cons_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)

# database
conn = sqlite3.connect('tweets.db')

time1 = None
while True:
    time2 = datetime.now()
    if time1 == None or (time2 - time1).seconds >= 60:
        print('Checking...')
        time1 = datetime.now()
        tweets = []
        num = 0
        for tweet in tweepy.Cursor(api.search,
                                    q='#yodifyme',
                                    count=200,
                                    result_type="recent",
                                    include_entities=True,
                                    lang="en").items():
            try:
                # tweet = cursor.next()
                tweets.append(tweet)
                num += 1
            except Exception as e:
                print(str(e))
                break
        tweets = [do_cleaning(t) for t in tweets if is_clean_tweet(t.text)]

        print('\nCleaned tweets:')
        for tw in tweets:
            print('\t"{}"'.format(tw.text))#, get_tweeters_username(tw), '\n')

        print()
        for tweet_choice in [t for t in tweets if is_tweet_new(conn, t)]:
            print('Yodifying:', tweet_choice.text, '(ID={})'.format(tweet_choice.id))
            yodified, matched = yodify.yodify(tweet_choice.text)
            if matched:
                print('\tResponding:', yodified)
                conn.execute("INSERT INTO TWEETS (ID) VALUES (" + str(tweet_choice.id) + ')')
                conn.commit()
                respond_to_tweet(api, tweet_choice, yodified)
            else:
                print('\tCould not yodify')
                conn.execute("INSERT INTO TWEETS (ID) VALUES (" + str(tweet_choice.id) + ')')
                conn.commit()
                respond_to_tweet(api, tweet_choice, yodify.TOO_WEIRD_MSG)

conn.close()
