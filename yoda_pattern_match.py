import yodify2 as yoda

verbose = False

def handle_text(parsed_text):
    try:
        for pattern in PATTERNS:
            if verbose:
                print('\n\nPATTERN:', pattern[2], pattern[0])
            if matches(pattern, parsed_text):
                return pattern[2](parsed_text), True
        return ' '.join(get_text(parsed_text)), False
    except:
        print('!!!! Something went wrong !!!!')
        return None, False

def matches_dep(pattern, parsed_text, ndx):
    if verbose:
        print('Pat:', pattern)
    for d in pattern.split('|'):
        if '+' not in d and (d in parsed_text[ndx].dep_ or d == parsed_text[ndx].text):
            return True, 1
        elif '+' in d:
            ordered = d.split('+')
            for i in range(len(ordered)):
                if ndx + i >= len(parsed_text):
                    return False, 0
                elif ordered[i] not in parsed_text[ndx + i].dep_ and ordered[i] != parsed_text[ndx + i].text:
                    return False, 0
            return True, len(ordered)
    return False, 0

def matches(pattern, parsed_text):
    # set up break conditions
    breaks = pattern[1]['breaks'] if 'breaks' in pattern[1] else []

    # CHECK DEP PATTERN
    cur_pat_ndx = 0
    dep_pattern = pattern[0]
    matches_list = lambda dep, li: len([d for d in li if dep in d]) > 0
    ndx = 0
    while ndx in range(len(parsed_text)):
        dep = parsed_text[ndx].dep_
        if cur_pat_ndx < len(dep_pattern):
            valid_dep_match, increase = matches_dep(dep_pattern[cur_pat_ndx], parsed_text, ndx)
            if valid_dep_match:
                # look for next dep in list if found one
                cur_pat_ndx += 1
                ndx += increase - 1
            elif matches_list(dep, dep_pattern):
                # doesn't match if gets to a dep too early
                if verbose:
                    print('BREAK 1 |', dep, dep_pattern, cur_pat_ndx)
                return False
            elif matches_list(dep, breaks):
                # hit break condition
                if verbose:
                    print('BREAK 2')
                return False
        ndx += 1
    # check if all deps in pattern used
    if cur_pat_ndx < len(dep_pattern):
        if verbose:
            print('BREAK 3')
        return False

    # CHECK SECONDARY CONDITIONS
    conds = pattern[1]
    num_dep = lambda query: len([t for t in parsed_text if query in t.dep_])
    # check dep counts
    for key in conds:
        if type(conds[key]) == int and key in DEP_KEYS:
            if num_dep(key) != conds[key]:
                if verbose:
                    print('BREAK 4')
                return False
    if 'more_words' in conds and len(parsed_text) <= conds['more_words']:
        return False
    if 'ndxs' in conds:
        if verbose:
            print('ndx reqs')
        ndx_dict = conds['ndxs']
        for ndx in conds['ndxs']:
            # part of speech
            if ndx_dict[ndx].upper() == ndx_dict[ndx]:
                if parsed_text[ndx].pos_ != ndx_dict[ndx]:
                    if verbose:
                        print('Fail:', ndx, ndx_dict[ndx], parsed_text[ndx].pos_)
                    return False
            elif ndx_dict[ndx] not in parsed_text[ndx].dep_ and ndx_dict[ndx] != parsed_text[ndx].text:
                if verbose:
                    print('Fail:', ndx, ndx_dict[ndx], parsed_text[ndx].dep_)
                return False

    return True

###################### HELPER FUNCTIONS ############################################

def root_ndx(parsed_text):
    return [i for i in range(len(parsed_text)) if parsed_text[i].dep_ == 'ROOT'][0]

def get_text(parsed_text):
    return [t.text for t in parsed_text]

def get_string(parsed_text):
    return ' '.join(get_text(parsed_text))

###################### CASE-SPECIFIC HANDLING FUNCTIONS ############################

def simple_swap(parsed_text):
    print('simple_swap')
    root = root_ndx(parsed_text)
    new_order = get_text(parsed_text[root:]) + [','] + get_text(parsed_text[:root])
    return ' '.join(new_order)

def split_sent(parsed_text):
    print('split_sent')
    # find point to break the phrase into two sentences
    for sp_point in range(root_ndx(parsed_text) + 1, len(parsed_text)):
        if parsed_text[sp_point].dep_ in ['cc', 'punct']:
            break
    # handle ", and" by skipping the and
    buf = 2 if parsed_text[sp_point + 1].dep_ == 'cc' else 1
    # yodify each sentence separately
    return ' '.join([
                yoda.yodify(get_string(parsed_text[:sp_point]))[0],
                yoda.yodify(get_string(parsed_text[sp_point + buf:]))[0]
            ])

def root_swap(parsed_text):
    print('root_swap')
    root = root_ndx(parsed_text)
    new_order = get_text(parsed_text[root+1:]) + [','] + get_text(parsed_text[:root+1])
    return ' '.join(new_order)

def move_first_two(parsed_text):
    print('move_first_two')
    new_order = get_text(parsed_text[2:]) + [','] + get_text(parsed_text[:2])
    return ' '.join(new_order)

def handle_because(parsed_text):
    print('handle_because')
    sp_point = [i for i in range(len(parsed_text)) if parsed_text[i].text == 'because'][0]
    return ' '.join([
                yoda.yodify(get_string(parsed_text[:sp_point]))[0],
                yoda.yodify(get_string(parsed_text[sp_point + 1:]))[0]
            ])

def handle_colon(parsed_text):
    print('handle_colon')
    sp_point = [i for i in range(len(parsed_text)) if parsed_text[i].text == ':'][0]
    return ' '.join([
                yoda.yodify(get_string(parsed_text[:sp_point]))[0],
                yoda.yodify(get_string(parsed_text[sp_point + 1:]))[0]
            ])

###################### PATTERNS TO CHECK ###########################################
DEP_KEYS = ['subj']

PATTERNS = [
    (['subj', 'aux', 'ROOT'],
        {'subj': 1, 'breaks': ['punct', 'cc']},
        simple_swap),
    (['subj', 'ROOT', 'cc|punct|punct+cc', 'subj'],
        {'subj': 2, 'breaks': ['punct', 'cc']},
        split_sent),
    (['subj', 'ROOT', 'punct+cc', 'subj'],
        {'subj': 2, 'breaks': ['punct', 'cc']},
        split_sent),
    (['subj+ROOT'],
        {'subj': 1, 'more_words': 2},
        root_swap),
    ([], {'ndxs':{0:'subj', 1:'VERB'}, 'more_words': 2},
        move_first_two),
    (['subj', 'because'], {'subj': 2, 'breaks': ['subj']},
        handle_because),
    (['subj', ':'], {'breaks': ['subj']},
        handle_colon),
    (['subj', 'ROOT'], {'subj': 1, 'more_words': 2},
        root_swap)
]
