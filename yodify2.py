import spacy
import contractions
import color_print as cp
import yoda_pattern_match as ypm

singled_out = None

nlp = spacy.load('en')
PUNC = '!?.'
ALL_PUNC = ':;.,"!@#$%^&*()-_=+?/\|[]{}`~' + "'"
TOO_WEIRD_MSG = 'Too weird, this sentence is. Fix this, I must. #YodaFail'

HASHTAG = ' #Yodified'

FUN_ANSWERS = {
    'shut the fuck up': 'Up the shut fuck!' + HASHTAG
}

def print_tree(parsed_text):
    for i in range(len(parsed_text)):
        word = parsed_text[i]
        col = None
        if 'subj' in word.dep_:
            col = cp.bcolors.GREEN
        elif 'ROOT' == word.dep_:
            col = cp.bcolors.RED
        elif word.dep_ in ['punct', 'cc']:
            col = cp.bcolors.YELLOW
        cp.pr(col, '\t', '{0: <3}'.format(i), '{0: <10}'.format(word.text), '\t', word.dep_, word.pos_)

def do_rearrange(parsed_text):
    output, matched = ypm.handle_text(parsed_text)
    if not matched or singled_out != None:
        print_tree(parsed_text)
    return output, matched

def yodify(text):
    text = text.lower().strip()
    for fun in FUN_ANSWERS:
        if fun in text:
            return FUN_ANSWERS[fun], True
    text = contractions.remove_contractions(text)

    # deal with ending punctuation
    end_punc = '.'
    if text[-1] in PUNC:
        end_punc = text[-1]
        text = text[:-1]

    # rearrange sentence
    output, matched = do_rearrange(nlp(text))

    if output == None or output[0] in ALL_PUNC:
        return TOO_WEIRD_MSG, False

    return finalize(output, end_punc), matched

def finalize(output, end_punc):
    # add ending punctuation back
    if output[-1] != end_punc:
        output += end_punc
    cap = lambda string: '. '.join([s.strip().capitalize() for s in string.split('.')])
    output = cap(output).replace(' i ', ' I ').strip()
    # add some yoda-isms
    if end_punc == '?':
        output += ' Hmmm?'
    output = contractions.punctuation_spacing(output) + HASHTAG
    output = output.replace('Sw ', 'SW ').replace('sw ', 'SW ').replace('can not', 'cannot')
    return output.replace('SW', 'Star Wars')

if __name__ == '__main__':
    import sys
    num = 1
    with open(sys.argv[1], 'r') as f:
        for line in f.readlines():
            if singled_out != None and num > singled_out:
                exit()
            print(num)
            num += 1
            if singled_out != None and num < singled_out + 1:
                continue
            if len(line.strip()) > 0:
                yodified, matched = yodify(line.strip())
                if '-w' not in sys.argv or not matched or singled_out != None:
                    print('\n', line.strip())
                    col = cp.bcolors.CYAN if matched else cp.bcolors.PURPLE
                    cp.pr(col, yodified)
                    print('\n--------------------------------------------------------\n')