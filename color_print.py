class bcolors:
    PURPLE = '\033[95m'
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

ENDC = '\033[0m'

def pr(color, *text):
	if color != None:
		print(color, *text, ENDC)
	else:
		print('', *text)