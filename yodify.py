import spacy
import contractions

nlp = spacy.load('en')

inputs = [
        'You will not look as good when you reach nine hundred years old',
        "I shot an elephant in my pajamas.",
        'The mind of a child is truly wonderful.',
        'Do you judge me by my size?'
    ]
    
PUNC = '!?.'

TOO_WEIRD_MSG = 'Too weird, this sentence is. Fix this, I must. #YodaFail'

def do_rearrange(parsed_text):
    subjs = len([x for x in parsed_text if 'subj' in x.dep_])
    if subjs > 2:
        return None
    if subjs > 0:
        for i in range(len(parsed_text)):
            print('\t', parsed_text[i].text, '|', parsed_text[i].dep_)
        return rearrange_mult_subject(parsed_text)
    return rearrange_single_subject(parsed_text)

def rearrange_mult_subject(parsed_text):
    # check for linking punctuation between subjects
    found_subj = False
    link = None
    for i in range(len(parsed_text)):
        if 'subj' in parsed_text[i].dep_:
            if found_subj:
                break
            found_subj = True
        elif 'punc' in parsed_text[i].dep_ and found_subj:
            link = i
            break
    # temp break if no punctuation found
    if link == None:
        return None
    # break into two sentences
    s1 = parsed_text[:link]
    s2 = parsed_text[link + 1:]
    print('S1:', rearrange_single_subject(s1))
    print(yodify(' '.join([t.text for t in s1])))
    print('S2:', rearrange_single_subject(s2))
    print(yodify(' '.join([t.text for t in s2])))
    return ' '.join([rearrange_single_subject(s1), rearrange_single_subject(s2)])

def rearrange_single_subject(parsed_text):
    # remove up to subject (to simplify)
    if 'subj' not in parsed_text[0].dep_:
        for i in range(len(parsed_text)):
            if 'subj' in parsed_text[i].dep_:
                parsed_text = parsed_text[i:]
                parsed_text = nlp(' '.join([t.text for t in parsed_text]))
                break
            if 'ROOT' == parsed_text[i].dep_:
                return TOO_WEIRD_MSG
    # find split point (root of sentence)
    root_ndx = None
    for i in range(len(parsed_text)):
        #print('\t', parsed_text[i].text, parsed_text[i].dep_)
        if parsed_text[i].dep_ == "ROOT":
            root_ndx = i + 1
    if root_ndx == None or root_ndx == 0:
        return TOO_WEIRD_MSG
    # do sentence rearranging
    strings = [str(t.text) for t in parsed_text]
    end = strings[: root_ndx]
    # deal with weird ending
    if end[-1].lower() == 'i':
        end.append('did')
    # combine sentence parts
    return ' '.join(strings[root_ndx :]) + ', ' + ' '.join(end)

def yodify(text):
    if '"' in text:
        return TOO_WEIRD_MSG
    if 'shut the fuck up' in text.lower():
        return 'Up the shut fuck! #Yodafied'
    text = contractions.remove_contractions(text).replace('...', '.')
    # deal with ending punctuation
    end_punc = '.'
    if text[-1] in PUNC:
        end_punc = text[-1]
        text = text[:-1]
    parsed_text = nlp(text)    
    
    output = do_rearrange(parsed_text)
    if output == None:
        return TOO_WEIRD_MSG
    # add ending punctuation back
    output += end_punc
    output = output.capitalize().replace(' i ', ' I ')
    # add some yoda-isms
    if end_punc == '?':
        output += ' Hmmm?'
    output = contractions.punctuation_spacing(output) + ' #Yodified'
    output = output.replace('star wars', 'Star Wars')
    return output

if __name__ == '__main__':
    import sys
    with open(sys.argv[1], 'r') as f:
        for line in f.readlines():
            if len(line.strip()) > 0:
                print(line.strip())
                print(yodify(line.strip()))
                print()